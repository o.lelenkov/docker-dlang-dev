FROM registry.gitlab.com/o.lelenkov/docker-minideb:buster-0.1
MAINTAINER Oleg Lelenkov "o.lelenkov@gmail.com"

ARG DMD_VERSION=2.086.1
ARG LLVM_VERSION=8
ARG LDC_COMMIT=v1.16.0
ARG DUB_VERSION=v1.16.0

RUN install_packages curl libc6-dev gcc \
        gnupg2 ca-certificates git cmake make g++ zlib1g-dev

COPY ./setup /tmp/setup

# indtall DMD
RUN sh /tmp/setup/dmd.sh

# install ldc2
RUN sh /tmp/setup/ldc2.sh

# install dub
RUN sh /tmp/setup/dub.sh

RUN apt-get -y autoremove && rm -rf /var/lib/apt/lists/* \
        && rm -rf /tmp/* && rm -rf ~/.dub

