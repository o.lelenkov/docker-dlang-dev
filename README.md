[![build status](https://gitlab.com/o.lelenkov/docker-dlang-dev/badges/master/build.svg)](https://gitlab.com/o.lelenkov/docker-dlang-dev/commits/master)

Образ содержит минимальный набор инструментов для разработки на [D](http://dlang.org/).

## Состав

* DMD
* LDC
* DUB

