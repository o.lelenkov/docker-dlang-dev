#!/bin/sh
set -e

CPUC=$(awk '/^processor/{n+=1}END{print n}' /proc/cpuinfo)

echo "deb http://apt.llvm.org/buster/ llvm-toolchain-buster-${LLVM_VERSION} main" >> /etc/apt/sources.list.d/llvm.list
echo "deb-src http://apt.llvm.org/buster/ llvm-toolchain-buster-${LLVM_VERSION} main" >> /etc/apt/sources.list.d/llvm.list

curl -fsSL https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -

install_packages clang-${LLVM_VERSION} llvm-${LLVM_VERSION}-dev \
    llvm-${LLVM_VERSION}-runtime llvm-${LLVM_VERSION}

git clone https://github.com/ldc-developers/ldc.git /tmp/ldc -b ${LDC_COMMIT}
cd /tmp/ldc
git submodule update --recursive --init

BUILD_FLAGS="-DBUILD_SHARED_LIBS:BOOL=ON"

if [ $LLVM_VERSION -ge 7 ]; then
    BUILD_FLAGS="$BUILD_FLAGS -DLLVM_CONFIG=/usr/bin/llvm-config-$LLVM_VERSION"
fi

mkdir build && cd build
cmake $BUILD_FLAGS  ..
make -j${CPUC} && make install

cd /
rm -rf /tmp/ldc

